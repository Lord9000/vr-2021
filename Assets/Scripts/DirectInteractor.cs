using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectInteractor : Interactor
{
    private Collider interactorCollider;

    public void Awake()
    {
        OnTouched += TouchInteractable;
        OnUntouched += UntouchInteractable;
        TouchedObjects = new List<Interactable>();
        InteractedObjects = null;
    }

    public override void InteractionStart()
    {
        if(InteractedObjects == null && TouchedObjects.Count > 0)
        {
            InteractedObjects = TouchedObjects[TouchedObjects.Count - 1];
            TouchedObjects.Remove(InteractedObjects);
            InteractedObjects.OnInteractionStart?.Invoke(this);
        }
    }

    public override void InteractionEnd()
    {
        if(InteractedObjects != null)
        {
            InteractedObjects.OnInteractionEnd?.Invoke(this);
            InteractedObjects = null;
        }
    }

    #region Events
    private void TouchInteractable(Interactable interactable)
    {
        TouchedObjects.Add(interactable);
    }

    private void UntouchInteractable(Interactable interactable)
    {
        TouchedObjects.Remove(interactable);
    }
    #endregion
}
