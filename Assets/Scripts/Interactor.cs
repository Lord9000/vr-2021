using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interactor : MonoBehaviour
{
    public List<Interactable> TouchedObjects { get; protected set; }
    public Interactable InteractedObjects { get; protected set; }

    public Action<Interactable> OnTouched;
    public Action<Interactable> OnUntouched;
    public Action<Interactable> OnInteracted;
    public Action<Interactable> OnUninteracted;

    public abstract void InteractionStart();
    public abstract void InteractionEnd();
}
