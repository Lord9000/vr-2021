using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Brick : MonoBehaviour
{
    public enum State { Undefined, Free, InHand, Locked }

    public List<Transform> Snaps = new List<Transform>();

    [SerializeField]
    private State _state = State.Free;
    private State _lastState = State.Undefined;
    public State CurrentState
    {
        get => _state;
        set
        {
            if (value != _lastState)
            {
                switch (_state = value)
                {
                    case State.InHand:
                        OnGrab();
                        break;

                    case State.Free:
                        OnRelease();
                        break;

                    case State.Locked:
                        OnLock();
                        break;
                }
                _lastState = _state;
                ChangeMaterial();
            }
        }
    }

    public Rigidbody RB { get; private set; }
    public Collider Collider { get; private set; }
    public Renderer Renderer { get; private set; }


    public Material LockMaterial;
    public Material InHandMaterial;
    public Material FreeMaterial;

    private float transparencyShift = 0.4f;
    private float currentTransparency = 1.0f;

    #region Unity
    private void Awake()
    {
        RB = GetComponent<Rigidbody>();
        Collider = GetComponent<Collider>();
        currentTransparency = 1.0f;
    }

    private void Start()
    {
        Renderer = GetComponentInChildren<Renderer>();
        if (_state == State.Locked)
            ForceLock();
    }
    #endregion

    private void ForceLock()
    {
        _state = _lastState = State.Locked;
        Collider.enabled = true;
        RB.isKinematic = true;
        Grid3D.instance.ForceAdd(this);
        ChangeMaterial();
    }

    private void OnLock()
    {
        if (_lastState == State.InHand)
        {
            Collider.enabled = true;
            RB.isKinematic = true;
            if (!Grid3D.instance.TryAdd(this))
            {
                CurrentState = State.Free;
            }
        }
        else
        {
            CurrentState = State.Free;
        }
    }

    private void OnGrab()
    {
        RB.velocity = Vector3.zero;
        RB.angularVelocity = Vector3.zero;
        RB.useGravity = false;
        Collider.enabled = false;
        Grid3D.instance.Remove(this);
    }

    private void OnRelease()
    {
        Collider.enabled = true;
        RB.isKinematic = false;
        RB.useGravity = true;
        Grid3D.instance.Remove(this);
    }

    private void ChangeMaterial()
    {
        switch (CurrentState)
        {
            case State.Free:
                Renderer.material = FreeMaterial;
                break;
            case State.InHand:
                Renderer.material = InHandMaterial;
                break;
            case State.Locked:
                Renderer.material = LockMaterial;
                break;
        }

        if (Application.isPlaying)
        {
            Color c = Renderer.material.color;
            c.a = currentTransparency;
            Renderer.material.color = c;
        }

    }

    public void Touch(Interactor interactor)
    {
        currentTransparency = 1.0f - transparencyShift;
        ChangeMaterial();
    }

    public void Untouch(Interactor interactor)
    {
        currentTransparency = 1.0f;
        ChangeMaterial();
    }
}
