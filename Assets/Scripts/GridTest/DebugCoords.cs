using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class DebugCoords : MonoBehaviour
{
    private Brick obj;
    private TextMeshProUGUI TM;
    private static readonly string fSpec = "0.0"; 


    // Start is called before the first frame update
    void Start()
    {
        TM = GetComponent<TextMeshProUGUI>();
        obj = GetComponentInParent<Brick>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 w = obj.transform.position;
        TM.text = $"W: [{w.x.ToString(fSpec)}, {w.y.ToString(fSpec)}, {w.z.ToString(fSpec)}]";

        foreach (var item in Grid3D.instance.WorldPositions(obj))
        {
            TM.text += $"\nG:[{ item.x}, { item.y}, { item.z}]";
        }
    }
}
