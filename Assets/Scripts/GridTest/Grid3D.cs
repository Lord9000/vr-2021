using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Grid3D : MonoBehaviour
{
    private static readonly Vector3[] axes = new Vector3[] { Vector3.right, Vector3.left, Vector3.up, Vector3.down, Vector3.forward, Vector3.back };

    public Vector3Int min;
    public Vector3Int max;

    private Dictionary<Vector3Int, Brick> pairs = new Dictionary<Vector3Int, Brick>();

    public static Grid3D instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void SnapToAxis(Transform brick)
    {
        Vector3 newForward = Vector3.zero;
        float cos = 0;

        foreach (var axis in axes)
        {
            var dot = Vector3.Dot(brick.forward, axis);
            if (cos < dot)
            {
                cos = dot;
                newForward = axis;
            }
        }

        brick.forward = newForward;
    }

    void Snap(Transform brick, Transform brickSnap)
    {
        Vector3 target = GridToWorld(WorldToGrid(brickSnap.position));

        SnapToAxis(brick);

        brick.transform.position += target - brickSnap.position;
    }

    public void ForceAdd(Brick obj)
    {
        Snap(obj.transform, obj.Snaps[0]);

        foreach (var snap in obj.Snaps)
        {
            pairs.Add(WorldToGrid(snap.position), obj);
        }
    }

    public bool TryAdd(Brick obj)
    {
        var possibleSnaps = GetPossibleSnaps(obj);
       
        if (possibleSnaps.Count == 0)
            return false;

        Snap(obj.transform, possibleSnaps[0]);

        foreach (var snap in obj.Snaps)
        {
            pairs.Add(WorldToGrid(snap.position), obj);
        }

        return true;
    }

    public void Remove(Brick obj)
    {
        foreach (var item in pairs.Where(kvp => kvp.Value == obj).ToList())
        {
            pairs.Remove(item.Key);
        }
    }

    public List<Transform> GetPossibleSnaps(Brick obj)
    {
        var list = new List<Transform>();

        foreach (var snap in obj.Snaps)
        {
            var gridPosition = WorldToGrid(snap.position);

            if (pairs.ContainsKey(gridPosition))
                return new List<Transform>();

            foreach (var neighbour in GetNeighbours(gridPosition))
            {
                if (pairs.ContainsKey(neighbour))
                    list.Add(snap);
            }
        }

        return list;
    }

    public IEnumerable<Vector3Int> WorldPositions(Brick brick)
    {
        foreach (var _transfotm in brick.Snaps)
        {
            yield return WorldToGrid(_transfotm.position);
        }
    }


    public Vector3Int WorldToGrid(Vector3 worldPosition)
    {
        // temp solution
        return Vector3Int.RoundToInt(worldPosition);
    }

    public Vector3 GridToWorld(Vector3Int worldPosition)
    {
        // temp solution
        return worldPosition;
    }

    private Vector3Int[] GetNeighbours(Vector3Int of)
    {
        return new Vector3Int[]
        {
            of + Vector3Int.up,         of + Vector3Int.down,
            of + Vector3Int.forward,    of + Vector3Int.back,
            of + Vector3Int.left,       of + Vector3Int.right
        };
    }


    private void DebugPrint()
    {
        var log = "Current Grid status:";

        foreach (var item in pairs)
        {
            log += $"\n{item.Key} - {item.Value}";
        }

        Debug.Log(log);
    }
}
