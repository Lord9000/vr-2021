using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    private Collider objectCollider;
    private Rigidbody objectRigidbody;
    private Transform sceneParent;
    private Material mat;
    private Brick brick;

    public List<Interactor> TouchedByInteractors { get; private set; }
    public Interactor InteractedByInteractor { get; private set; }

    public Action<Interactor> OnTouched;
    public Action<Interactor> OnUntouched;
    public Action<Interactor> OnInteractionStart;
    public Action<Interactor> OnInteractionEnd;

    private void Awake()
    {
        InteractedByInteractor = null;
        TouchedByInteractors = new List<Interactor>();
        objectCollider = GetComponent<Collider>();
        objectRigidbody = GetComponent<Rigidbody>();
        mat = GetComponentInChildren<Renderer>().material;
        brick = GetComponent<Brick>();
        //gameObject.layer = LayerMask.NameToLayer("Interactable");

        OnInteractionStart += InteractionStart;
        OnInteractionEnd += InteractionEnd;
        OnTouched += brick.Touch;
        OnUntouched += brick.Untouch;

        sceneParent = transform.parent;
    }

    private void OnDestroy()
    {
        OnInteractionStart -= InteractionStart;
        OnInteractionEnd -= InteractionEnd;
        OnTouched -= brick.Touch;
        OnUntouched -= brick.Untouch;
    }

    #region Collider
    private void OnTriggerEnter(Collider other)
    {
        Interactor interactor = other.gameObject.GetComponent<DirectInteractor>();
        if (interactor != null)
        {
            OnTouched.Invoke(interactor);
            interactor.OnTouched?.Invoke(this);
            TouchedByInteractors.Add(interactor);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Interactor interactor = other.gameObject.GetComponent<DirectInteractor>();
        if (interactor != null)
        {
            OnUntouched.Invoke(interactor);
            interactor.OnUntouched?.Invoke(this);
            bool removed = TouchedByInteractors.Remove(interactor);
            if (!removed) Debug.LogError("Interactor not found!");
        }
    }
    #endregion

    #region Events
    private void InteractionStart(Interactor interactor)
    {
        if(InteractedByInteractor == null)
        {
            InteractedByInteractor = interactor;
            transform.parent = interactor.transform;
            brick.CurrentState = Brick.State.InHand;
        }
    }

    private void InteractionEnd(Interactor interactor)
    {
        if(InteractedByInteractor == interactor)
        {
            InteractedByInteractor = null;
            transform.parent = sceneParent;
            brick.CurrentState = Brick.State.Locked;
        }
        else
        {
            throw new Exception("Wrong interactor");
        }
    }

    #endregion
}
