using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR;

public class TestVrSimulator : MonoBehaviour
{
    public bool simulatorEnabled = true;
    
    public GameObject vrRig;
    public GameObject head;
    public GameObject leftHand;
    public GameObject rightHand;

    public InputActionAsset simulatorControls;

    private InputAction movement;
    private InputAction look;

    private bool simulate;
    private Vector2 movementVector;
    private Quaternion headRotation;

    private void Awake()
    {
        simulate = simulatorEnabled && !XRSettings.enabled;

        if (simulate)
        {
            movement = simulatorControls.FindAction("Movement");
            movement.performed += OnMovementChanged;
            movement.canceled += OnMovementChanged;
            movement.Enable();

            look = simulatorControls.FindAction("Look");
            look.performed += OnLookChanged;
            look.canceled += OnLookChanged;
            look.Enable();
        }
    }

    private void OnLookChanged(InputAction.CallbackContext context)
    {
        
    }

    private void OnMovementChanged(InputAction.CallbackContext context)
    {
        movementVector = context.ReadValue<Vector2>();
    }

    private void Update()
    {
        if (simulate)
        {
            vrRig.transform.position += new Vector3(movementVector.x, 0.0f, movementVector.y);
        }
    }
}
