using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Movable : MonoBehaviour
{
    private Vector2 mouseXY;
    private float mouseZ;

    private Vector3 translate;
    private Quaternion rotate;


    public float MouseZmin => mouseZ = Camera.main.nearClipPlane + 0.1f;

    private void Start()
    {
        rotate = Quaternion.identity;
        mouseZ = MouseZmin;
    }

    private void Update()
    {
        transform.rotation *= rotate;
        transform.position += transform.rotation * translate;
    }

    public void MovetoMouseZ(InputAction.CallbackContext context)
    {
        mouseZ = Mathf.Max(mouseZ + context.ReadValue<float>(), MouseZmin);
        ApplyMouse();
    }

    public void MoveToMouse(InputAction.CallbackContext context)
    {
        mouseXY = context.ReadValue<Vector2>();
        ApplyMouse();
    }

    private void ApplyMouse()
    {
        Vector3 screenPos = (Vector3)mouseXY + Vector3.forward * mouseZ;
        Vector3 worldPos = Camera.main.ScreenToWorldPoint(screenPos);
        transform.position = worldPos;
    }

    public void MoveByKeyboard(InputAction.CallbackContext context)
    {
        Vector2 input = context.ReadValue<Vector2>();
        translate = new Vector3(input.x / 10, 0, input.y / 10);
    }

    public void Rotate(InputAction.CallbackContext context)
    {
        rotate = Quaternion.AngleAxis(context.ReadValue<float>(), Vector3.up);
    }
}
