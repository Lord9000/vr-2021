using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TMP_SNAP_SEEDER : MonoBehaviour
{
    public Vector3Int snaps;

    [ContextMenu("Generate snaps")]
    private void GenerateSnaps()
    {
        bool brickFound;
        if (brickFound = TryGetComponent<Brick>(out var brick))
        {
            brick.Snaps.Clear();
        }
        var root = Instantiate(new GameObject("Snaps"), transform);

        for (int x = 0; x < snaps.x; x++)
        {
            for (int y = 0; y < snaps.y; y++)
            {
                for (int z = 0; z < snaps.z; z++)
                {
                    if (x == 0 || y == 0 || z == 0 || x == snaps.x-1 || y == snaps.y-1 || z == snaps.z-1)
                    {
                        var snap = Instantiate(new GameObject($"Snap{x}.{y}.{z}"), root.transform);
                        snap.transform.position += new Vector3(x, y, z);
                        snap.transform.position -= (snaps - Vector3.one) * 0.5f;

                        if (brickFound)
                            brick.Snaps.Add(snap.transform);
                    }
                }
            }
        }
    }
}
